package id.aashari.code.miband2.model;
import java.sql.Date;
public class Data{
        private String id;
        private Date date;
        private String pulse;
        private String step;
        private String battery;
        private String distance;
        private String kcal;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public String getPulse() {
            return pulse;
        }

        public void setPulse(String pulse) {
            this.pulse = pulse;
        }

        public String getStep() {
            return step;
        }

        public void setStep(String step) {
            this.step = step;
        }

        public String getBattery() {
            return battery;
        }

        public void setBattery(String battery) {
            this.battery = battery;
        }

        public Data(String id, Date date, String pulse, String step, String battery) {
            super();
            this.id = id;
            this.date = date;
            this.pulse = pulse;
            this.step = step;
            this.battery = battery;
        }

        public Data() {
        }
}
